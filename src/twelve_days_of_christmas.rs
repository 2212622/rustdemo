const OBJECTS_GIVEN: [&str; 12] = [
    "A Partridge in a Pear Tree",
    "Two Turtle Doves",
    "Three French Hens",
    "Four Calling Birds",
    "Five Gold Rings",
    "Six Geese A-Laying",
    "Seven Swans A-Swimming",
    "Eight Maids A-Milking",
    "Nine Ladies Dancing",
    "Ten Lords A-Leaping",
    "Eleven Pipers Piping",
    "Twelve Drummers Drumming",
];

fn main() {
    let mut count = 1;

    while count <= OBJECTS_GIVEN.len() {
        println!("{}\n", generate_lyrics(count));
        count = count + 1;
    }
}

///this function gives the lyrics of 12 days of christmas
///it returns a string based on the number/day that was given
fn generate_lyrics(day: usize) -> String {
    let suffix;

    if day == 1 {
        suffix = "st";
    } else if day == 2 {
        suffix = "nd";
    } else if day == 3 {
        suffix = "rd";
    } else {
        suffix = "th";
    }

    let mut lyrics = format!(
        "On the {}{} day of Christmas my true love gave to me,\n",
        day, suffix
    );

    let mut object_count = day;
    if day == 1 {
        // a partridge in a pear tree.
        lyrics.push_str(OBJECTS_GIVEN[0]);
        lyrics.push_str(".");
    } else {
        while object_count != 0 {
            if object_count == 1 {
                lyrics.push_str("And ");
                lyrics.push_str(OBJECTS_GIVEN[object_count as usize - 1]);
                lyrics.push_str(".");
            } else {
                lyrics.push_str(OBJECTS_GIVEN[object_count as usize - 1]);
                lyrics.push_str(",\n");
            }

            object_count = object_count - 1;
        }
    }
    lyrics
}

mod test {
    #[test]
    #[should_panic]
    fn test_basic() {
        assert_eq!(super::generate_lyrics(13), "On the 1st day of Christmas my true love gave to me,\nA Partridge in a Pear Tree.");
    }
}
