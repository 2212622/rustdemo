extern crate pick_one;
use pick_one::pick_one_str;

fn main() {
    // christmas-related emojis
    let list = [
        "🎅", "🤶", "🎄", "⛄", "☃", "❄", "🌟", "🦌", "🔔", "🎶", "✨",
    ];

    let emoji = pick_one_str(&list);
    print!("{}", emoji);
}
